use std::error::Error;
use std::fmt;
use std::fmt::Display;

static BACKSLASH_LOWERCASE: &[&str] = &[
    "", "", "", "", "", "", "", "ɦ", "", "ʝ", "", "ɺ", "", "", "", "ɸ", "", "ɹ", "ɕ", "",
    "ʋ", "", "ɧ", "", "ʑ",
];

static RETRO_LOWERCASE: &[&str] = &[
    "", "", "", "ɖ", "", "", "", "", "", "", "", "ɭ", "", "ɳ", "", "", "", "ɽ", "ʂ", "ʈ", "",
    "", "", "", "", "ʐ",
];

static IMPLOSIVE_LOWERCASE: &[&str] = &["", "ɓ", "", "ɗ", "", "", "ɠ"];

static CAPITAL: &[&str] = &[
    "ɑ", "β", "ç", "ð", "ɛ", "ɱ", "ɣ", "ɥ", "ɪ", "ɲ", "ɬ", "ʎ", "ɯ", "ŋ", "ɔ", "ʋ",
    "ɒ", "ʁ", "ʃ", "θ", "ʊ", "ʌ", "ʍ", "χ", "ʏ", "ʒ",
];

static BACKSLASH_CAPITAL: &[&str] = &[
    "", "ʙ", "", "", "", "", "ɢ", "ʜ", "ᵻ", "ɟ", "ɮ", "ʟ", "ɰ", "ɴ", "ʘ", "", "", "ʀ",
    "", "", "ʊ̈", "", "", "ħ", "", "",
];

static SYMBOLS: &[&str] = &[
    // start with space
    " ", "ꜜ", "ˈ", "#", "$", "ˌ", "ɶ", "ʲ", "(", ")", "*", "+", ",", "", ".", "/", "0", "ɨ",
    "ø", "ɜ", "ɾ", "ɫ", "ɐ", "ɤ", "ɵ", "œ", "ː", ";", "<", "̩", ">", "ʔ", "ə",
];

static UDIA: &[&str] = &[
    // start with "
    "̈", "", "", "", "", "", "", "", "", "̟", "", "", "", "̌", "̥", "", "", "", "", "", "", "",
    "", "", "", "", "", "̩", "ʼ", "", "", // A
    " ̘", "̏", "", "", "", "̂", "ˠ", "́", "", "", "", "̀", " ̄", "̼", "̹", "", "", "̌",
    "", "̋", "", "", "", "̆", "", "", // Z
    "", "", "", "", "", "", // a
    "̺", "", "̜", "̪", "̴", "", "", "ʰ", "", "ʲ", "̰", "ˡ", "̻", "ⁿ", "̞", "", "̙",
    "̝", "", "̤", "", "̬", "ʷ", "̽", "", "", // z
    "", "", "̚", "̃",
];

#[inline]
fn index(s: &[char], i: usize) -> char {
    if i >= s.len() {
        '\0'
    } else {
        s[i]
    }
}

#[inline]
fn subchar(a: char, b: char) -> usize {
    (a as usize) - (b as usize)
}

#[derive(Debug)]
pub struct ConvertError {
    ty: ConvertErrorType,
    so_far: String,
}

impl ConvertError {
    pub fn new(ty: ConvertErrorType, so_far: String) -> ConvertError {
        ConvertError { ty, so_far }
    }
}

impl Display for ConvertError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::ConvertErrorType::*;
        match self.ty {
            BackslashCantModify(c) => write!(f, "Backslash can't modify {}", c),
            ImplosiveCantModify(c) => write!(f, "_< can't modify {}", c),
            LoneUnderscoreAtEnd => f.write_str("Lone underscore at end"),
            UnrecognisedDiacritic(c) => write!(f, "No diacritic with symbol {}", c),
            UnrecognisedSymbol(c) => write!(f, "No such symbol {}", c),
            Aaaaaa => f.write_str("ああああああ！"),
        }?;
        write!(f, " (converted so far: {})", self.so_far)
    }
}

impl Error for ConvertError {}

#[derive(Debug)]
pub enum ConvertErrorType {
    BackslashCantModify(char),
    ImplosiveCantModify(char),
    LoneUnderscoreAtEnd,
    UnrecognisedDiacritic(char),
    UnrecognisedSymbol(char),
    Aaaaaa,
}

pub fn convert(s: &str) -> Result<String, ConvertError> {
    /*
        This code was translated from the C++ VaneBot code,
        so it might not be the most idiomatic. Sorry.
    */
    use self::ConvertErrorType::*;
    let chars: Vec<char> = s.chars().collect();
    let mut i = 0usize;
    let mut result = String::new();
    while i < chars.len() {
        let c = chars[i];
        let mut j = i + 1;
        let mut backslash = false;
        let mut retro = false;
        if index(&chars, j) == '\\' {
            backslash = true;
            j += 1;
        }
        if index(&chars, j) == '`' {
            // Note: no else
            retro = true;
            j += 1;
        }
        if c >= 'A' && c <= 'Z' {
            if retro {
                j -= 1;
            }
            if !backslash {
                result.push_str(CAPITAL[subchar(c, 'A')]);
            } else {
                let sym = BACKSLASH_CAPITAL[subchar(c, 'A')];
                if sym.is_empty() {
                    return Err(ConvertError::new(BackslashCantModify(c), result));
                }
                result.push_str(sym);
            }
        } else if c >= 'a' && c <= 'z' {
            if retro {
                if backslash {
                    if c == 'r' {
                        result.push_str("ɻ");
                    } else {
                        j -= 1;
                        retro = false;
                    }
                } else {
                    let sym = RETRO_LOWERCASE[subchar(c, 'a')];
                    if sym.is_empty() {
                        j -= 1;
                        retro = false;
                    } else {
                        result.push_str(sym);
                    }
                }
            }
            if
            /* still */
            retro {
                i = j;
                continue;
            }
            if backslash {
                let sym = BACKSLASH_LOWERCASE[subchar(c, 'a')];
                if sym.is_empty() {
                    return Err(ConvertError::new(BackslashCantModify(c), result));
                }
                result.push_str(sym);
            } else if index(&chars, j) == '_' && index(&chars, j + 1) == '<' {
                // implosive
                if c >= 'h' || IMPLOSIVE_LOWERCASE[subchar(c, 'a')].is_empty() {
                    return Err(ConvertError::new(ImplosiveCantModify(c), result));
                }
                result.push_str(IMPLOSIVE_LOWERCASE[subchar(c, 'a')]);
                j += 2;
            } else {
                result.push(c);
            }
        } else if c == '_' {
            if j < chars.len() {
                let mut should_continue = true;
                let t: &[char] = &chars[j..];
                if t.starts_with(&['B', '_', 'L']) {
                    result.push_str("᷅");
                } else if t.starts_with(&['H', '_', 'T']) {
                    result.push_str("᷄");
                } else if t.starts_with(&['R', '_', 'F']) {
                    result.push_str("᷈")
                } else {
                    should_continue = false;
                }
                if should_continue {
                    i = j + 3;
                    continue;
                }
            }
            let d = index(&chars, j);
            if d == '\0' {
                return Err(ConvertError::new(LoneUnderscoreAtEnd, result));
            }
            // This check wasn't in the original -- DOH!
            if d > '~' {
                return Err(ConvertError::new(UnrecognisedDiacritic(d), result));
            }
            let sym = UDIA[subchar(d, '\x22')];
            if sym.is_empty() {
                return Err(ConvertError::new(UnrecognisedDiacritic(d), result));
            } else {
                result.push_str(sym);
                j += 1;
            }
        } else if c < 'A' {
            if j < chars.len() {
                let mut should_continue = true;
                let t: &[char] = &chars[j - 1..];
                if t.starts_with(&['<', 'F', '>']) {
                    result.push_str("\\↘");
                } else if t.starts_with(&['<', 'R', '>']) {
                    result.push_str("\\↗");
                } else {
                    should_continue = false;
                }
                if should_continue {
                    i = j + 2;
                    continue;
                }
            }
            if retro {
                if c == '@' {
                    result.push_str("ɚ");
                } else {
                    j -= 1;
                    retro = false;
                }
            }
            if
            /* still */
            retro {
                i = j;
                continue;
            }
            if !backslash {
                result.push_str(SYMBOLS[subchar(c, ' ')]);
            } else {
                let sym = match c {
                    ':' => "ˑ",
                    '@' => "ɘ",
                    '3' => "ɞ",
                    '?' => "ʕ",
                    '<' => "ʢ",
                    '>' => "ʡ",
                    '!' => "ǃ",
                    '=' => "ǂ",
                    '-' => "‿",
                    _ => {
                        return Err(ConvertError::new(BackslashCantModify(c), result));
                    }
                };
                result.push_str(sym);
            }
        } else {
            match c {
                '^' => {
                    result.push_str("ꜛ");
                }
                '|' => {
                    if backslash {
                        if index(&chars, j) == '|' && index(&chars, j + 1) == '\\' {
                            j += 2;
                            result.push_str("ǁ");
                        } else {
                            result.push_str("ǀ");
                        }
                    } else if index(&chars, j) != '|' {
                        result.push_str("|");
                    } else {
                        result.push_str("‖");
                        j += 1;
                    }
                }
                '{' => result.push_str("æ"),
                '}' => result.push_str("ʉ"),
                '~' => result.push_str("̃"),
                '`' => result.push_str("˞"),
                _ => {
                    return Err(ConvertError::new(UnrecognisedSymbol(c), result));
                }
            }
        }
        i = j;
    }
    Ok(result)
}
