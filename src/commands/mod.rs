use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::fmt::Display;

pub struct CommandArgs {
    pub strings: Vec<String>,
    pub ints: Vec<i64>,
    pub floats: Vec<f64>,
}

impl CommandArgs {
    fn new() -> Self {
        CommandArgs {
            strings: Vec::new(),
            ints: Vec::new(),
            floats: Vec::new(),
        }
    }
}

pub type Handler<T, U> = for<'a> fn(&'a mut T, CommandArgs) -> U;
pub type Label = String;

pub enum CommandTree<T, U> {
    Accept(Handler<T, U>),
    TakeUnquoted(Handler<T, U>, Label),
    TakeInt(Box<CommandTree<T, U>>, Option<i64>, Label),
    TakeFloat(Box<CommandTree<T, U>>, Option<f64>, Label),
    TakeString(Box<CommandTree<T, U>>, Option<String>, Label),
    TakeSubcommand(
        HashMap<String, Box<CommandTree<T, U>>>,
        Option<Handler<T, U>>,
    ),
}

impl<T, U> CommandTree<T, U> {
    pub fn accept(h: Handler<T, U>) -> Self {
        CommandTree::Accept(h)
    }
    pub fn unquoted(l: Label, h: Handler<T, U>) -> Self {
        CommandTree::TakeUnquoted(h, l)
    }
    pub fn take_int(l: Label, t: CommandTree<T, U>) -> Self {
        CommandTree::TakeInt(Box::new(t), None, l)
    }
    pub fn take_int_default(l: Label, t: CommandTree<T, U>, d: i64) -> Self {
        CommandTree::TakeInt(Box::new(t), Some(d), l)
    }
    pub fn take_float(l: Label, t: CommandTree<T, U>) -> Self {
        CommandTree::TakeFloat(Box::new(t), None, l)
    }
    pub fn take_float_default(l: Label, t: CommandTree<T, U>, d: f64) -> Self {
        CommandTree::TakeFloat(Box::new(t), Some(d), l)
    }
    pub fn take_string(l: Label, t: CommandTree<T, U>) -> Self {
        CommandTree::TakeString(Box::new(t), None, l)
    }
    pub fn take_string_default(l: Label, t: CommandTree<T, U>, d: String) -> Self {
        CommandTree::TakeString(Box::new(t), Some(d), l)
    }
}

#[derive(Debug)]
pub enum CommandError {
    Nyi,
    TooMany,
    QuotedString,
    NoInput,
    NumberFormat,
    BadOption,
}

impl Display for CommandError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::CommandError::*;
        f.write_str(match self {
            Nyi => "not yet implemented",
            TooMany => "too many arguments",
            QuotedString => "quoted string failed to parse",
            NoInput => "not enough arguments",
            NumberFormat => "invalid number format",
            BadOption => "bad option",
        })
    }
}

impl Error for CommandError {}

fn unescape_string(input: &str) -> Option<(String, &str)> {
    let mut result = String::new();
    let mut chars = input.chars();
    loop {
        match chars.next() {
            None => return None, // need an ending "
            Some('"') => {
                return Some((result, chars.as_str()));
            }
            Some('\\') => match chars.next() {
                None => return None,
                Some('n') => result.push('\n'),
                Some('t') => result.push('\t'),
                Some('\\') => result.push('\\'),
                Some('\"') => result.push('\"'),
                Some(c) => result.push(c),
            },
            Some(c) => result.push(c),
        }
    }
}

fn next_word(input: &str) -> Option<(String, &str)> {
    // Assumes that we've passed any spaces.
    if input.starts_with('\"') {
        return unescape_string(&input[1..]);
    }
    if input.is_empty() {
        return None;
    }
    // Take next word
    for (i, c) in input.char_indices() {
        if c.is_whitespace() {
            return Some((String::from(&input[0..i]), &input[i..]));
        }
    }
    Some((String::from(input), ""))
}

pub fn evaluate<T, U>(tree: &CommandTree<T, U>, cmd: &str, t: &mut T) -> Result<U, CommandError> {
    use self::CommandError::*;
    use self::CommandTree::*;
    let mut cur_node: &CommandTree<T, U> = tree;
    let mut next_input: &str = cmd;
    let mut args = CommandArgs::new();
    loop {
        // Skip spaces
        next_input = next_input.trim_start();
        match *cur_node {
            Accept(ref h) => {
                if next_input.is_empty() {
                    return Ok((*h)(t, args));
                } else {
                    return Err(TooMany);
                }
            }
            TakeUnquoted(ref h, _) => {
                if next_input.starts_with('\"') {
                    match unescape_string(&next_input[1..]) {
                        None => return Err(QuotedString),
                        Some((quoted, remaining)) => {
                            if !remaining.is_empty() {
                                return Err(TooMany);
                            } else {
                                args.strings.push(String::from(next_input.trim()));
                            }
                        }
                    }
                } else {
                    args.strings.push(String::from(next_input.trim()));
                }
                return Ok((*h)(t, args));
            }
            TakeString(ref tree, ref def, _) => match (next_word(next_input), def) {
                (None, None) => return Err(NoInput),
                (None, Some(ref d)) => args.strings.push(d.clone()),
                (Some((s, next)), _) => {
                    args.strings.push(s);
                    next_input = next;
                    cur_node = tree;
                }
            },
            TakeInt(ref tree, ref def, _) => match (next_word(next_input), def) {
                (None, None) => return Err(NoInput),
                (None, Some(d)) => args.ints.push(*d),
                (Some((s, next)), _) => match s.as_str().parse::<i64>() {
                    Err(_) => return Err(NumberFormat),
                    Ok(n) => {
                        args.ints.push(n);
                        next_input = next;
                        cur_node = tree;
                    }
                },
            },
            TakeFloat(ref tree, ref def, _) => match (next_word(next_input), def) {
                (None, None) => return Err(NoInput),
                (None, Some(d)) => args.floats.push(*d),
                (Some((s, next)), _) => match s.as_str().parse::<f64>() {
                    Err(_) => return Err(NumberFormat),
                    Ok(n) => {
                        args.floats.push(n);
                        next_input = next;
                        cur_node = tree;
                    }
                },
            },
            TakeSubcommand(ref m, ref d) => match next_word(next_input) {
                None => return Err(NoInput),
                Some((s, next)) => match (m.get(&s), d) {
                    (None, None) => return Err(BadOption),
                    (None, Some(h)) => {
                        args.strings.push(s);
                        return Ok((*h)(t, args));
                    }
                    (Some(tree), _) => {
                        next_input = next;
                        cur_node = tree;
                    }
                },
            },
            _ => return Err(Nyi),
        }
    }
    Err(TooMany)
}

fn doc_help<T, U>(tree: &CommandTree<T, U>, out: &mut Vec<String>, prefix: &str) {
    use self::CommandTree::*;
    match tree {
        Accept(_) => {
            out.push(String::from(prefix));
        },
        TakeUnquoted(_, l) => {
            let new_label = format!("{} <{}:str*>", prefix, l);
            out.push(new_label);
        },
        TakeString(subtree, d, l) => {
            let new_label = match d {
                None => format!("{} <{}:str>", prefix, l),
                Some(d) => format!("{} <{}:str={}>", prefix, l, d),
            };
            doc_help(subtree, out, new_label.as_str());
        },
        TakeInt(subtree, d, l) => {
            let new_label = match d {
                None => format!("{} <{}:int>", prefix, l),
                Some(d) => format!("{} <{}:int={}>", prefix, l, d),
            };
            doc_help(subtree, out, new_label.as_str());
        },
        TakeFloat(subtree, d, l) => {
            let new_label = match d {
                None => format!("{} <{}:real>", prefix, l),
                Some(d) => format!("{} <{}:real={}>", prefix, l, d),
            };
            doc_help(subtree, out, new_label.as_str());
        },
        TakeSubcommand(m, _) => {
            let mut v: Vec<_> = m.iter().collect();
            v.sort_unstable_by(|a, b| {
                a.0.cmp(b.0)
            });
            for (k, subtree) in v {
                doc_help(subtree, out, (String::from(prefix) + " " + k).as_str());
            }
        }
    }
}

pub fn doc<T, U>(tree: &CommandTree<T, U>, prefix: &str) -> String {
    let mut out = Vec::<String>::new();
    doc_help(tree, &mut out, "");
    let mut s = String::new();
    for line in out {
        s.push_str(prefix);
        s.push_str(line.trim());
        s.push('\n');
    }
    s
}
