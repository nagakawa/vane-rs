extern crate aho_corasick;
extern crate chrono;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate maplit;
extern crate ramp;
extern crate rand;
extern crate regex;
#[macro_use]
extern crate serenity;

mod commands;
mod filtcheck;
mod xsampa2ipa;

use std::fs::File;
use std::io::Read;
use std::process::exit;
use std::rc::Rc;

use chrono::prelude::*;
use ramp::int::Int;
use regex::Regex;
use serenity::{
    model::{channel::{Message, ReactionType}, gateway::{Game, Ready}, id::{ChannelId, EmojiId, GuildId}},
    prelude::*,
};

use commands::CommandTree;

static INFO: &str = "\
I am VaneBot, written by Uruwi / kozet.
I was created for use in the AGC Discord server.
I am programmed in ~~C++~~ Rust, using serenity \\<<https://github.com/serenity-rs/serenity>>.
Check out my code at \\<<https://gitlab.com/nagakawa/vane-rs>>.
(Hint: use `x/.../` to convert X-SAMPA to IPA!)";
static INFO_APRIL: &str = "\
hAI me Iz vANeBot! me iZ oohrUDEwhee's dAUgHter!!!
oOhRudeWHEE :spoon:ed oNE DaY iN le BEEGEEVEE SERVER!!! OMG!!!
lolololol HAHA Me iz progRAMMed in ~~SEE PLUS PLUS~~ DA BEST LANGUAGE EVAR, RUST!!! OMG kan u believes it???
kthxbye"
    ;
static KH: &str = "\
ridia! ridia! ridia! ridiaaaaaaaaaaaaaaaaaaaaaaaaaannnnnnnnn!!!!!!!!!!
aaaaaaaaa.......aaaa. aa...aaaggg!! ridia, ridia, ridiaaaaaaaaaaaaaaaaannnnnnnnnn!!!!!!!
aaaaa, kuh kuh! kuuh kuuh!! suh suh! suuh suuh!! aca ti et liito aa... son son.
ahaaa! an toan lax nia leje e ridia lutia liij fo kuuh kuuh aaa!!! kuuh kuuh! aaa!!!!
teo! an oj lax nia laat fo muf muf! mof mof, muuf muuf! nia, nia! muf muf! kyan kyan kyun!
ridia liij kaen ardia keno xelt e milf at ank tinka dac! aaaaaaa...aaaa...ap, aaaaaa! oooouuuuuuuugggg!
an na nau on ti sil nok a slax 5 t'avelantis, ridia liij! aaaaaa! ank, ank e! ridia liij! ank!!! ap, aaaaaa!!!
vei kalfia as at atm ento an na nau ati va--- teeeeeeeeeeeeeeeeeeee! teoooooooooooo!!! aaaaaaaaaaaaaarteeeeeeeeee!!!
haaaaaaaaaaaaaaaaaaaaaaizeeeeeeeeeeeeennnn! kalfia tis de fiana daaaaaaaaaa!!!!!! ap, an xaklik yul avelantis tan et........
RIDIA LIIJ et en fiana??????????????? teeeoooooooooooooooooo!!!!!! aaaaaaaaaaaaaaaaa!!!!!!
aaarteeeeeeeeeeeee! kleeeeeeeeeeeeeeveeeeeeeeeeeeeeeelllllll! aaaaaaaaaaaaaaaaaaannnn! aaaaaaaaarbazaaaaaaaaaaard!!!!
ocaaaaaaa! beo daaaaaa!!!!!! an leev! an leev van tuube fia tis-- to?? lu inor...? ridia liiz kaen olmaleis inor an xa!!!
ridia liiz kaen avelantis xookor a men da!!!! atta... tu fia des ges xa!! (xante
hwaaaaaaaaaaaaaaaa!!!!! an til ridia liiz!!!!! atta al an, mel! men so sen xed netal eu!
ou, ridia liiz kaen kalfiaaaaaaaaaaaaaaa! teooooooooooooooooooooooooo!
aaaaaaaa, aaaaaaag, ap, ap, liiza xanxaaaaaaaa! k...kmiiiiiiiirrrr! luxiaaaaaaaaaaaa!!!!!! miliaaaaaaaaa!!!!!!
u....uuuuuuuuuuuuooooooooooooo!!!!!!! der xiilas leyute fiina ti, ridia. alem ant, ti re luko sil ridia kaen arbazard!"
    ;

lazy_static! {
    static ref XSAMPA_TAG: Regex = Regex::new("x/([^/]+)/").unwrap();
    static ref CMD_TREE: CommandTree<CommandContext, ()> = CommandTree::TakeSubcommand(
        hashmap!{
            String::from("about") => Box::new(CommandTree::accept(|ctx: &mut CommandContext, _args| {
                let local = Local::now();
                let date = local.date();
                let msg = if date.month() == 4 && date.day() == 1 {
                    INFO_APRIL
                } else {
                    INFO
                };
                try_to_send(ctx.msg.channel_id, msg);
            })),
            String::from("help") => Box::new(CommandTree::accept(|ctx: &mut CommandContext, _args| {
                try_to_send(ctx.msg.channel_id, &format!("```\n{}```", *CMD_HELP));
            })),
            String::from("kh") => Box::new(CommandTree::accept(|ctx: &mut CommandContext, _args| {
                try_to_send(ctx.msg.channel_id, KH);
            })),
            String::from("bc") => Box::new(
                CommandTree::take_int(
                    String::from("from"), CommandTree::take_int(
                        String::from("to"), CommandTree::take_string(
                            String::from("value"), CommandTree::accept(|ctx: &mut CommandContext, args| {
                                let from = args.ints[0];
                                let to = args.ints[1];
                                if from < 2 || from > 36 || to < 2 || to > 36 {
                                    try_to_send(ctx.msg.channel_id, "Bases must be in range [2, 36]");
                                    return;
                                }
                                let from = from as u8;
                                let to = to as u8;
                                let st = args.strings[0].as_str();
                                let n = match Int::from_str_radix(st, from) {
                                    Err(e) => {
                                        try_to_send(ctx.msg.channel_id, &format!("Could not parse number: {}", e));
                                        return;
                                    },
                                    Ok(n) => n
                                };
                                let result = n.to_str_radix(to, true);
                                try_to_send(ctx.msg.channel_id, result.as_str());
                            }
                        )
                    )
                )
            )),
            String::from("barf") => Box::new(CommandTree::accept(|ctx: &mut CommandContext, _args| {
                let res = if rand::random() {
                    ctx.msg.react(ReactionType::Unicode("\u{1F922}".to_string()))
                } else {
                    ctx.msg.react(ReactionType::Custom {
                        animated: false,
                        id: EmojiId::from(505827486496587796u64),
                        name: Some("bakan_bekam".to_string()),
                    })
                };
                if let Err(e) = res {
                    println!("Error reacting: {:?}", e);
                }
            })),
            String::from("cat") => Box::new(CommandTree::unquoted(String::from("contents"), |ctx: &mut CommandContext, args| {
                try_to_send(ctx.msg.channel_id, &format!("```\n{}\n```", args.strings[0]));
            })),
            String::from("canisay") => Box::new(CommandTree::unquoted(String::from("phrase"), |ctx: &mut CommandContext, args| {
                let can_say = filtcheck::FILTER.can_say(&args.strings[0]);
                let msg = if can_say {
                    "Go ahead and say it loud!"
                } else {
                    "It shall not be said!"
                };
                try_to_send(ctx.msg.channel_id, msg);
            })),
        },
        Some(|ctx: &mut CommandContext, mut args| {
            try_to_send(
                ctx.msg.channel_id,
                &format!("I don't know how to {}", args.strings.pop().unwrap()),
            );
        })
    );
    static ref CMD_HELP: String = commands::doc(&CMD_TREE, "&");
    static ref MY_GUILD: GuildId = GuildId::from(99960841276768256u64);
}

struct CommandContext {
    msg: Rc<Message>, // temp hack until Rust gets HKTs like C++
}

struct Handler {
    //g: Rc<Guild>,
}

fn try_to_send(chan: ChannelId, msg: &str) {
    if let Err(why) = chan.say(msg) {
        println!("Error sending message: {:?}", why);
    }
}

fn curse_at_sender(msg: &Message) {
    let nick = msg.author_nick();
    let nick = nick.unwrap_or(msg.author.name.clone());
    try_to_send(msg.channel_id, &format!("Fuck off, {}.", nick));
}

impl EventHandler for Handler {
    fn message(&self, _: Context, msg: Message) {
        if msg.is_own() {
            return;
        }
        let guild_id = msg.guild_id;
        let msg = Rc::new(msg);
        for caps in XSAMPA_TAG.captures_iter(msg.content.as_str()) {
            let s = match xsampa2ipa::convert(caps.get(1).unwrap().as_str()) {
                Ok(ipa) => ipa,
                Err(e) => format!("X-SAMPA→IPA conversion failed: {}", e),
            };
            try_to_send(msg.channel_id, s.as_str());
        }
        if msg.content.starts_with('&') {
            match guild_id {
                None => {
                    if let Err(e) = MY_GUILD.member(msg.author.id) {
                        curse_at_sender(&msg);
                        return;
                    }
                },
                Some(s) if s == *MY_GUILD => (), // Meant for this
                Some(s) => {
                    curse_at_sender(&msg);
                    if let Err(e) = s.leave() {
                        println!("Error leaving: {}", e);
                    }
                    return;
                }
            }
            let mut ctx = CommandContext { msg: msg.clone() };
            let mut over = msg.content.chars();
            over.next().unwrap();
            if let Err(e) = commands::evaluate(&CMD_TREE, over.as_str(), &mut ctx) {
                try_to_send(msg.channel_id, &format!("{:?}", e));
            }
        }
    }

    fn ready(&self, c: Context, ready: Ready) {
        c.set_game(Game::playing("Type &help"));
        println!("{} is connected!", ready.user.name);
    }
}

fn main() {
    let mut f = match File::open("token") {
        Ok(f) => f,
        Err(e) => {
            println!("Couldn't open file `token`: {}", e);
            exit(-1);
        }
    };
    let mut token = String::new();
    match f.read_to_string(&mut token) {
        Ok(_) => (),
        Err(e) => {
            println!("Couldn't read token from file: {}", e);
            exit(-1);
        }
    }
    let mut client = match Client::new(&token, Handler {}) {
        Ok(client) => client,
        Err(e) => {
            println!("Couldn't create client: {}", e);
            exit(-1);
        }
    };
    match client.start() {
        Ok(_) => (),
        Err(e) => {
            println!("Couldn't start client: {}", e);
            exit(-1);
        }
    }
}
