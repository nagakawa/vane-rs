use std::collections::{HashMap, HashSet};
use std::convert::From;
use std::error::Error;
use std::fmt;
use std::fmt::Display;
use std::fs;
use std::fs::File;
use std::io::{BufRead, BufReader};

use aho_corasick::{Automaton, AcAutomaton, Match};
use regex::Regex;

#[derive(Debug)]
pub enum FilterError {
    ReplacementFileParse(String),
    ReadError(std::io::Error),
}

impl Display for FilterError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::FilterError::*;
        match self {
            ReplacementFileParse(s) => write!(f, "parsing replacement file failed: `{}` is not a valid line", s),
            ReadError(e) => e.fmt(f),
        }
    }
}

impl Error for FilterError {}

impl From<std::io::Error> for FilterError {
    fn from(error: std::io::Error) -> Self {
        FilterError::ReadError(error)
    }
}

#[derive(Debug)]
pub struct Redlist {
    whole_words: AcAutomaton<String>,
    inner: AcAutomaton<String>,
    inner_with_asterisks: AcAutomaton<String>,
    phrases: AcAutomaton<String>,
}

impl Redlist {
    pub fn new(fh: fs::File) -> Result<Redlist, FilterError> {
        let mut whole_words = Vec::<String>::new();
        let mut inner = Vec::<String>::new();
        let mut inner_with_asterisks = Vec::<String>::new();
        let mut phrases = Vec::<String>::new();
        for line in BufReader::new(fh).lines() {
            match line {
                Ok(line) => {
                    if line.is_empty() { continue; }
                    let line = line.to_lowercase();
                    let line = line.trim();
                    if line.starts_with('-') && line.ends_with('-') {
                        let line = line.trim_matches('-');
                        if line.contains('*') {
                            inner_with_asterisks.push(line.to_string());
                        } else {
                            inner.push(line.to_string());
                        }
                    } else if line.contains(' ') {
                        phrases.push(line.to_string());
                    } else {
                        whole_words.push(line.to_string());
                    }
                },
                Err(e) => {
                    return Err(FilterError::ReadError(e));
                }
            }
        }
        Ok(Redlist {
            whole_words: AcAutomaton::new(whole_words),
            inner: AcAutomaton::new(inner),
            inner_with_asterisks: AcAutomaton::new(inner_with_asterisks),
            phrases: AcAutomaton::new(phrases),
        })
    }
}

#[derive(Debug)]
pub struct Filter {
    redlist: Redlist,
    exceptions: HashSet<String>,
    replacements: HashMap<char, String>,
}

fn is_nonce(c: char) -> bool {
    "~`!#%^&()-_=+\\|;:'\",<.>/?".contains(c)
}

fn is_on_whole_word(s: &str, m: &Match) -> bool {
    (m.end == s.len() || s.as_bytes()[m.end] == b' ') &&
        (m.start == 0 || s.as_bytes()[m.start - 1] == b' ')
}

fn word_containing<'a>(s: &'a str, m: &Match) -> &'a str {
    let mut i = m.start;
    let mut j = m.end;
    while i > 0 && s.as_bytes()[i - 1] != b' ' {
        i -= 1;
    }
    while j < s.len() && s.as_bytes()[j] != b' ' {
        j += 1;
    }
    return &s[i..j];
}

impl Filter {
    pub fn new(red: fs::File, exc: fs::File, rep: fs::File) -> Result<Filter, FilterError> {
        let exceptions: HashSet<String> = BufReader::new(exc).lines().collect::<Result<HashSet<String>, _>>()?;
        let mut replacements = HashMap::<char, String>::new();
        for line in BufReader::new(rep).lines() {
            match line {
                Ok(orig_line) => {
                    let line = orig_line.trim().to_string();
                    if line.is_empty() || line.starts_with('#') {
                        continue;
                    }
                    match REPLACEMENT_REGEX.captures(&line) {
                        None => return Err(FilterError::ReplacementFileParse(String::from(orig_line))),
                        Some(c) => {
                            let key = &c[1];
                            let values = c[2].chars();
                            for value in values {
                                replacements.insert(value, String::from(key));
                            }
                        }
                    }
                },
                Err(e) => return Err(FilterError::ReadError(e)),
            }
        }
        Ok(Filter {
            redlist: Redlist::new(red)?,
            exceptions,
            replacements,
        })
    }
    pub fn can_say(&self, s: &str) -> bool {
        let substituted = {
            let mut res: String = String::new();
            for c in s.to_lowercase().chars() {
                match self.replacements.get(&c) {
                    Some(r) => res.push_str(r),
                    None => res.push(c),
                }
            }
            res
        };
        let substituted_without_nonces_with_asterisks = substituted.chars()
            .filter(|c| !is_nonce(*c))
            .collect::<String>();
        let substituted_without_nonces_without_asterisks = substituted_without_nonces_with_asterisks.chars()
            .filter(|c| *c != '*')
            .collect::<String>();
        println!("{}", &substituted);
        println!("{}", &substituted_without_nonces_with_asterisks);
        println!("{}", &substituted_without_nonces_without_asterisks);
        for m in self.redlist.whole_words.find(&substituted_without_nonces_without_asterisks) {
            if is_on_whole_word(&substituted_without_nonces_without_asterisks, &m) {
                return false;
            }
        }
        for m in self.redlist.phrases.find(&substituted) {
            if is_on_whole_word(&substituted, &m) {
                return false;
            }
        }
        for m in self.redlist.inner_with_asterisks.find(&substituted_without_nonces_with_asterisks) {
            return false;
        }
        for m in self.redlist.inner.find(&substituted_without_nonces_without_asterisks) {
            if !self.exceptions.contains(word_containing(&substituted_without_nonces_without_asterisks, &m)) {
                return false;
            }
        }
        true
    }
}

lazy_static! {
    static ref REPLACEMENT_REGEX: Regex = Regex::new("(\\S+)\\s*=\\s*(\\S+)").unwrap();
    pub static ref FILTER: Filter = Filter::new(
        File::open("filters/redlist.txt").unwrap(),
        File::open("filters/exceptions.txt").unwrap(),
        File::open("filters/replacements.txt").unwrap(),
    ).unwrap();
}